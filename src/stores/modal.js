import { computed, ref } from 'vue'
import { defineStore } from 'pinia'

export const useModalStore = defineStore(
  'modal',
  () => {
    const isOpen = ref(false)
    const hiddenClass = computed(() => {
      return isOpen.value ? '' : 'hidden'
    })
    return { isOpen, hiddenClass }
  }
)
